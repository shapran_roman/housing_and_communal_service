from django.contrib import admin

from .models import House, Apartment, Resident
# Register your models here.


class ResidentInline(admin.TabularInline):
    model = Resident


@admin.register(Apartment)
class ApartmentAdmin(admin.ModelAdmin):
    inlines = [
        ResidentInline
    ]


class ApartmentInline(admin.TabularInline):
    model = Apartment


@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    inlines = [
        ApartmentInline,
    ]

