from django.db import migrations


# TODO: set privileges to this user group
def apply_migration(apps, schema_editor):
    db_alias = schema_editor.connection.alias

    Group = apps.get_model("auth", "Group")
    Group.objects.using(db_alias).create(name="admin")


def revert_migration(apps, schema_editor):
    Group = apps.get_model("auth", "Group")
    Group.objects.get(name="admin").delete()


class Migration(migrations.Migration):
    dependencies = [("housing", "0004_auto_20210401_1613")]
    operations = [migrations.RunPython(apply_migration, revert_migration)]
