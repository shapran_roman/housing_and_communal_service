from django.db import models

# Create your models here.


class House(models.Model):
    CONCRETE = "Concrete"
    WOOD = "Wood"
    Brick = "Brick"
    MATERIAL_CHOICES = (
        (CONCRETE, "Concrete"),
        (WOOD, "Wood"),
        (Brick, "Brick"),
    )

    address = models.TextField(max_length=500, unique=True)
    material = models.CharField(max_length=15, choices=MATERIAL_CHOICES)
    company = models.CharField(max_length=30)


class Apartment(models.Model):
    house = models.ForeignKey(House, on_delete=models.DO_NOTHING)
    number = models.PositiveSmallIntegerField(default=1)
    price = models.IntegerField()
    square = models.IntegerField()
    rooms_count = models.IntegerField()

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["house", "number"], name="unique apartment number to house")
        ]


class Resident(models.Model):
    MAN = "M"
    WOMEN = "W"

    SEX_CHOICES = (
        (MAN, "M"),
        (WOMEN, "W"),
    )

    apartment = models.ForeignKey(Apartment, on_delete=models.DO_NOTHING)
    first_name = models.CharField(max_length=30)
    second_name = models.CharField(max_length=30)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES)
    age = models.PositiveSmallIntegerField()
    salary = models.PositiveSmallIntegerField(default=None, null=True, blank=True)

