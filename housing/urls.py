from django.urls import path

from . import views

app_name = "housing"

urlpatterns = [
    path("list", views.HousesList.as_view(), name="house-list"),
    path("<int:pk>/detail", views.HouseDetail.as_view(), name="house-detail")
]