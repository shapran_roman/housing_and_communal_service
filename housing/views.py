from django.views.generic import ListView, DetailView

from .models import House, Apartment, Resident

# Create your views here.


class HousesList(ListView):
    model = House
    context_object_name = "houses"


class HouseDetail(DetailView):
    model = House
    context_object_name = "house"

    def get_context_data(self, **kwargs):
        context = super(HouseDetail, self).get_context_data(**kwargs)
        context["apartments"] = Apartment.objects.filter(house_id=self.kwargs["pk"])
        return context


