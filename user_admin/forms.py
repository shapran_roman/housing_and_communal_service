from django import forms
from housing.models import Apartment, Resident


class ApartmentForm(forms.ModelForm):
    class Meta:
        model = Apartment
        exclude = ["house"]


class ResidentForm(forms.ModelForm):
    class Meta:
        model = Resident
        exclude = ["apartment"]