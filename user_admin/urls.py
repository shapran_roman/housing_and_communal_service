from django.urls import path

from . import views

app_name = "user_admin"

urlpatterns = [
    path("house/list", views.HouseList.as_view(), name="house-list"),
    path("house/create", views.CreateHouse.as_view(), name="house-create"),
    path("house/update/<int:pk>", views.UpdateHouse.as_view(), name="house-update"),
    path("house/<int:pk>/apartment/create", views.CreateApartment.as_view(), name="apartment-create"),
    path("house/apartment/<int:pk>/update", views.UpdateApartment.as_view(), name="apartment-update"),
    path("house/apartment/<int:pk>/resident/create", views.CreateResident.as_view(), name="resident-create")
]