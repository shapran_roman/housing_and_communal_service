from django.views.generic import CreateView, UpdateView, ListView
from django.shortcuts import reverse
from django.contrib.auth.mixins import UserPassesTestMixin
from housing.models import House, Apartment, Resident
from .forms import ApartmentForm, ResidentForm
# Create your views here.


class AdminGroutPermissionMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.groups.filter(name="admin").exists()


class HouseList(AdminGroutPermissionMixin, ListView):
    model = House
    context_object_name = "houses"
    template_name = "user_admin/house_list.html"


class CreateHouse(AdminGroutPermissionMixin, CreateView):
    model = House
    fields = "__all__"
    template_name = "user_admin/house_form.html"
    context_object_name = "house"


class UpdateHouse(AdminGroutPermissionMixin, UpdateView):
    model = House
    fields = "__all__"
    template_name = "user_admin/house_form.html"

    def get_success_url(self):
        return reverse("user_admin:house-update", kwargs={"pk": self.kwargs["pk"]})


class CreateApartment(AdminGroutPermissionMixin, CreateView):
    model = Apartment
    template_name = "user_admin/apartment_form.html"
    form_class = ApartmentForm

    def form_valid(self, form):
        form.instance.house = House.objects.get(pk=self.kwargs["pk"])
        return super(CreateApartment, self).form_valid(form)

    def get_success_url(self):
        return reverse("user_admin:house-update", kwargs={"pk": self.kwargs["pk"]})


class UpdateApartment(AdminGroutPermissionMixin, UpdateView):
    model = Apartment
    template_name = "user_admin/apartment_form.html"
    form_class = ApartmentForm

    def form_valid(self, form):
        form.instance.house = House.objects.get(pk=self.kwargs["pk"])
        return super(UpdateApartment, self).form_valid(form)

    def get_success_url(self):
        return reverse("user_admin:apartment-update", kwargs={"pk": self.kwargs["pk"]})


class CreateResident(AdminGroutPermissionMixin, CreateView):
    model = Resident
    template_name = "user_admin/resident_form.html"
    form_class = ResidentForm

    def form_valid(self, form):
        form.instance.apartment = Apartment.objects.get(pk=self.kwargs["pk"])
        return super(CreateResident, self).form_valid(form)

    def get_success_url(self):
        return reverse("user_admin:apartment-update", kwargs={"pk": self.kwargs["pk"]})
